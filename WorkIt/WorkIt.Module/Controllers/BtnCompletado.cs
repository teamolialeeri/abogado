﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using WorkIt.Module.BusinessObjects.WorkIt;

namespace WorkIt.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class BtnCompletado : ViewController
    {
        public BtnCompletado()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void btnValidar_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (Tarea a in e.SelectedObjects)
            {
                var tarea = (from c in new XPQuery<Tarea>(((XPObjectSpace)ObjectSpace).Session) where c.Oid == a.Oid select c).SingleOrDefault();
                var EstadoNuevo = (from c in new XPQuery<Estado>(((XPObjectSpace)ObjectSpace).Session) where c.Descripcion == "Completado" select c).SingleOrDefault();
                if (a.FechaFin == Convert.ToDateTime("1/1/0001") && tarea.Oid != 0 && EstadoNuevo.Oid != 0)
                {
                    a.FechaFin = DateTime.Now;
                    tarea.Estado = EstadoNuevo;
                }
            }
            View.ObjectSpace.CommitChanges();
            View.ObjectSpace.Refresh();
        }
    }
}
