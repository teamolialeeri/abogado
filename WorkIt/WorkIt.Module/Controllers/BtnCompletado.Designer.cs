﻿namespace WorkIt.Module.Controllers
{
    partial class BtnCompletado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnValidar = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // btnValidar
            // 
            this.btnValidar.Caption = "btn Validar";
            this.btnValidar.ConfirmationMessage = null;
            this.btnValidar.Id = "btnValidar";
            this.btnValidar.TargetObjectType = typeof(WorkIt.Module.BusinessObjects.WorkIt.Tarea);
            this.btnValidar.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.btnValidar.ToolTip = null;
            this.btnValidar.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.btnValidar.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.btnValidar_Execute);
            // 
            // BtnCompletado
            // 
            this.Actions.Add(this.btnValidar);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction btnValidar;
    }
}
