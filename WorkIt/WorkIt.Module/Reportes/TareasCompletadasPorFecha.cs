﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace WorkIt.Module.Reports
{
    public partial class TareasCompletadasPorFecha : DevExpress.XtraReports.UI.XtraReport
    {
        public TareasCompletadasPorFecha()
        {
            InitializeComponent();
        }

        private void TareasCompletadasPorFecha_ParametersRequestBeforeShow(object sender, DevExpress.XtraReports.Parameters.ParametersRequestEventArgs e)
        {
            var fromDate = new System.DateTime(System.DateTime.Today.Year, System.DateTime.Today.Month, 1);
            var toDate = fromDate.AddMonths(1).AddDays(-1);
            this.Parameters["FechaD"].Value = fromDate;
            this.Parameters["FechaH"].Value = toDate;
        }
    }
}
