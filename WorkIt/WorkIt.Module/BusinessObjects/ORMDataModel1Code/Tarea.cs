﻿using System;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp;
using System.Windows.Forms;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using DevExpress.XtraEditors;
using DevExpress.ExpressApp.Security.Strategy;
using System.Net.Mail;
using System.Net;
using System.Linq;

namespace WorkIt.Module.BusinessObjects.WorkIt
{

    public partial class Tarea
    {
        #region Variables
        public SecuritySystemUser AsignadoPor => Session.GetObjectByKey<SecuritySystemUser>(SecuritySystem.CurrentUserId);

        SecuritySystemUser fAsignadoA;
        public SecuritySystemUser AsignadoA
        {
            get { return fAsignadoA; }
            set { SetPropertyValue<SecuritySystemUser>(nameof(AsignadoA), ref fAsignadoA, value); }
        }

        SecuritySystemUser fAsignadoAOld;
        public SecuritySystemUser AsignadoAOld
        {
            get { return fAsignadoAOld; }
            set { SetPropertyValue<SecuritySystemUser>(nameof(AsignadoAOld), ref fAsignadoAOld, value); }
        }

        private FileData archivo;

        [Aggregated, ExpandObjectMembers(ExpandObjectMembers.Never)]
        public FileData Archivo
        {
            get { return archivo; }
            set { SetPropertyValue(nameof(Archivo), ref archivo, value); }
        }
        #endregion

        public Tarea(Session session) : base(session) { }
        public override void AfterConstruction()
        {
            base.AfterConstruction();

            var EstadoNuevo = (from i in new XPQuery<Estado>(Session) where i.Descripcion == "Sin Completar" select i).SingleOrDefault();

            Jerarquia = 1;
            if (EstadoNuevo != null) Estado = EstadoNuevo;
            FechaInicio = DateTime.Now;
        }

        protected override void OnSaving()
        {
            CreadoPor = (AsignadoPor.Oid);

            if (Estado.Descripcion == "Sin Completar")
            {
                if (AsignadoAOld == null || AsignadoA != AsignadoAOld)
                {
                    Email(/*AsignadoA*/);
                    AsignadoAOld = AsignadoA;
                }
            }
            base.OnSaving();
        }

        public void Email(/*Usuario*/)
        {
            if (AsignadoA != null)
            {
                if (Estado.Descripcion == "Sin Completar")
                {
                    var fromAddress = new MailAddress("mecaicuandoerachiquito@gmail.com", "From Name");
                    var toAddress = new MailAddress("mecaicuandoerachiquito@gmail.com", "To Name");
                    const string fromPassword = "sisisisi";
                    const string subject = "Subject";
                    const string body = "Body";

                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                        Timeout = 20000
                    };
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = subject,
                        Body = body
                    })
                    {
                        smtp.Send(message);
                    }
                }
            }

        }
    }
}
